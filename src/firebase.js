import * as firebase from "firebase";
import "firebase/database";

let config = {
    apiKey: "AIzaSyDGBAz_HOopU-p54upbVgY_pnFRW_fEngk",
    authDomain: "miguel-mixx-b054e.firebaseapp.com",
    databaseURL: "https://miguel-mixx-b054e-default-rtdb.firebaseio.com",
    projectId: "miguel-mixx-b054e",
    storageBucket: "miguel-mixx-b054e.appspot.com",
    messagingSenderId: "359056742851",
    appId: "1:359056742851:web:d46a886906818bfba94576",
    measurementId: "G-MLNHF36DTH"
};

firebase.initializeApp(config);

export default firebase.database();