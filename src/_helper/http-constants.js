import axios from 'axios';

let baseURL = process.env.baseURL || "https://djjaylyricsapp.com/djjaylyricsapp/api";

let instance = axios.create({
	baseURL
});

export const HTTP = instance;